var seneca = require('seneca')();
var _ = require('underscore');
var redis = require('redis');
var client = redis.createClient({host: 'redis'});

seneca.add({role: 'rating', cmd: 'getById'}, function(msg, respond) {
  client.hgetall('ratings-1234', function(error, results) {
    if (error) {
      respond(error);
    } else {
      respond(null, _.mapObject(results, function(value) {
        return(parseInt(value));
      }));
    }
  });
});

var samples = require('./samples.js');
samples.load(client, function(err) {
  if (err) {
    console.log('ERROR: rating-service - ' + err);
  } else {
    seneca.listen();
    seneca.ready(function() {
      console.log('rating-service ready!');
    });
  }
});
